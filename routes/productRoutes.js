const express = require("express")
const router = express.Router()
const ProductController = require("../controllers/ProductController")
const auth = require("../auth")


// Get all ACTIVE Products
router.get("/active", (request, response) => {
	ProductController.getAllActive().then(result => response.send(result))
})

// Get all  Products
router.get("/", (request, response) => {
	ProductController.getAllProducts().then(result => response.send(result))
})

// Get single Product
router.get("/:id", (request, response) => {
	ProductController.getProduct(request.params.id).then(result => response.send(result))
})

// Create new Product
router.post("/:id/create-product", auth.verify, (request, response) => {
	ProductController.createProduct(request.params.id,request.body).then(result => response.send(result))
})

// Get single Product
router.get("/:id", (request, response) => {
	ProductController.getProduct(request.params.id).then(result => response.send(result))
})

// Update details of existing Product
router.patch("/:user_id/:product_id/update", auth.verify, (request, response) => {
	ProductController.updateProduct(request.params.user_id, request.params.product_id, request.body).then(result => response.send(result))
})

// Activation of existing Product
router.patch("/:user_id/:product_id/activate", auth.verify, (request, response) => {
	ProductController.activateProduct(request.params.user_id, request.params.product_id, request.body).then(result => response.send(result))
})

// Archive a Product
router.patch("/:user_id/:product_id/archive", auth.verify, (request, response) => {
	ProductController.archiveProduct(request.params.user_id, request.params.product_id).then(result => response.send(result))
})


module.exports = router
