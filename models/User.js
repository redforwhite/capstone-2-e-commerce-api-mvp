const mongoose = require("mongoose")

const user_schema = new mongoose.Schema({
	
	firstName: {
		type: String,
		required: [true, "First name is required"]
	}, 

	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	mobileNo: String,

	isAdmin: {
		type: Boolean,
		default: false
	}, 

	addToCartProduct: [{

		products: [{
				productId:{
					type: String,
					required: [true, "Product ID is required"]
				},
				productName: {
					type: String,
					required: [true, "Product name is required"]
				},
				quantity: Number
			}],

			totalAmount: {
				type: Number,
				required: [true, "Total amount is required"]
			},

			purchasedOn: {
				type: Date,
			default: new Date()
			}
	}],

	orderedProduct: [{

			products: [{
				productId:{
					type: String,
					required: [true, "Product ID is required"]
				},
				productName: {
					type: String,
					required: [true, "Product name is required"]
				},
				quantity: Number
			}],

			totalAmount: {
				type: Number,
				required: [true, "Total amount is required"]
			},

			purchasedOn: {
				type: Date,
			default: new Date()
			}
			
		}]
})

module.exports = mongoose.model("User", user_schema)

