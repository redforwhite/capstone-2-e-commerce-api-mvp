const User = require("../models/User");
const Products = require("../models/Products");
const auth = require('../auth');
const bcrypt = require("bcrypt");

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then(result => {
		if(result.length > 0){
			return true 
		} 

		return false
	})
}

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10), //hashSync function will 'hash' the password and turn it into random characters. The second argument serves as the amount of times that the password will be hashed. Note: The higher the number, the slower it is to 'unhash' which will affect application performance, and the lower the number the faster it will be.
		mobileNo: request_body.mobileNo
	})

	return new_user.save().then((registered_user, error) => {
		if(error){
			return error 
		} 

		return 'User successfully registered!'
	})
}

module.exports.loginUser = (request_body) => {
	return User.findOne({email: request_body.email}).then(result => {
		if(result === null){
			return "The user doesn't exist."
		}

		// We can't use regular comparison to check if password is correct because the password of the existing user is hashed with bcrypt. We have to use bcrypt to de-hash the password and compare it to the password from the request body. The compareSync() function will return either true or false depending on if they match.
		const is_password_correct = bcrypt.compareSync(request_body.password, result.password)

		if(is_password_correct){
			return {
				// Using the createAccessToken function, we can generate a token using the user data after its password has been validated.
				accessToken: auth.createAccessToken(result.toObject())
			}
		}

		return 'The email and password combination is not correct!'
	})
}

// For getting user details from the token
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		return result
	})
}

// Update user to Admin
module.exports.updateUser = (user_id, new_content) => {
	let updated_user = {
		isAdmin: new_content.isAdmin
	}

	return User.findByIdAndUpdate(user_id, updated_user).then((modified_user, error) => {
		if(error){
			return error 
		}

		return {
			message: "User updated successfully!",
			data: modified_user
		}
	})
}