const Product = require("../models/Products");
const User = require("../models/User");

// Get all ACTIVE Products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(active_products => {
		return active_products
	})
}

// Get all Products
module.exports.getAllProducts = () => {
	return Product.find({}).then((courses, error) => {
		if(error){
			return error 
		}

		return courses
	})
}

// Get single Product
module.exports.getProduct = (product_id) => {
	return Product.findById(product_id).then(result => {
		return result 
	})
}

// Create a Product if User is an Admin
module.exports.createProduct = (user_id, new_item) => {
	return User.findById(user_id).then((found_user) => {

		if (!found_user.isAdmin){
			return {
				message: "User is not an Admin"
			}
		}

		let new_product = new Product({
			name: new_item.name,
			description: new_item.description,
			price: new_item.price,
		})

		return new_product.save().then((created_product) => {
			return {
			message: "Product created successfully!",
			data: created_product
			}
		})

	})
}


// Update a Product if User is an Admin
module.exports.updateProduct = (user_id, product_id, new_content) => {
	return User.findById(user_id).then((found_user) => {

		if (!found_user.isAdmin){
			return {
				message: "User is not an Admin"
			}
		}

		let updated_product = {
				name: new_content.name,	
				description: new_content.description,
				price: new_content.price,
				isActive: new_content.isActive
		}

		return Product.findByIdAndUpdate(product_id, updated_product).then((modified_product, error) => {
		if (error){
				return error 
			}

		return {
				message: "Product updated successfully!",
				data: modified_product
			}
		})

	})
}

// Activation of Product if User is an Admin
module.exports.activateProduct = (user_id, product_id, new_content) => {
	return User.findById(user_id).then((found_user) => {

		if (!found_user.isAdmin){
			return {
				message: "User is not an Admin"
			}
		}

		let activate_product = {
				isActive: new_content.isActive
		}

		return Product.findByIdAndUpdate(product_id, activate_product).then((activated_product, error) => {
		if (error){
				return error 
			}

		return {
				message: "Product activate successfully!",
				data: activated_product
			}
		})

	})
}


// Archive of Products if User is an Admin
module.exports.archiveProduct = (user_id, product_id) => {
	return User.findById(user_id).then((found_user) => {

		if (!found_user.isAdmin){
			return {
				message: "User is not an Admin"
			}
		}

		return Product.findById(product_id).then((product, error) => {
		if (error){
				return error
			}

		product.isActive = false

		return product.save().then((archived_product, error) => {
			if (error){
				return error 
			}

				return archived_product

			})
		})
	})
}